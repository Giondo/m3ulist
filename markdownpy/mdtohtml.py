import markdown

# md_text = """
# ```python hl_lines="1 3"
# # some Python code
# hi = 'Hello'
# print(hi)
# ```
# """

f = open("README.md", "r")
md_text = f.read()

html = markdown.markdown(md_text, extensions=['fenced_code', 'codehilite'])
# print(html)

f = open("README.html", "w")
f.write("<link rel=\"stylesheet\" href=\"static/css/codehilite.css\"/>\n")
f.write(html)
f.close()
